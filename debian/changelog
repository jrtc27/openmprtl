openmprtl (5.0-2) unstable; urgency=medium

  * Fix the symbol file for arm64

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 25 Nov 2017 18:33:22 +0100

openmprtl (5.0-1) unstable; urgency=medium

  * New upstream release (LP: #1717127)
  * New symbols:
    - __kmpc_atomic_fixed1u_sub_rev_fp
    - __kmpc_atomic_fixed2u_sub_rev_fp
    - __kmpc_atomic_fixed4u_sub_rev_fp
    - __kmpc_atomic_fixed8u_sub_rev_fp
    - __kmpc_task_reduction_get_th_data
    - __kmpc_task_reduction_init
  * Standards-Version updated to 4.1.1
  * priority-extra-is-replaced-by-priority-optional fixed
  * Add a "Suggests: libomp-doc" from the dev lib (Closes: #849605)

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 25 Nov 2017 14:43:05 +0100

openmprtl (4.0.1-4) unstable; urgency=medium

  * One more try to fix armhf & armel

 -- Sylvestre Ledru <sylvestre@debian.org>  Wed, 26 Jul 2017 07:39:56 +0200

openmprtl (4.0.1-3) unstable; urgency=medium

  * arm64 is fixed, try to fix armhf & armel (Closes: #868619)

 -- Sylvestre Ledru <sylvestre@debian.org>  Wed, 26 Jul 2017 07:39:46 +0200

openmprtl (4.0.1-2) unstable; urgency=medium

  * Try to fix the arm* symbols (Closes: #868619)

 -- Sylvestre Ledru <sylvestre@debian.org>  Fri, 21 Jul 2017 15:33:36 +0200

openmprtl (4.0.1-1) unstable; urgency=medium

  * New upstream release
  * Fix the mips64el FTBFS. Thanks to James Cowgill for the patch
    (Closes: #865111)
  * Fix the symbol issues (Closes: #865125)
  * Standards-Version => 4.0.0

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 15 Jul 2017 18:19:33 +0200

openmprtl (4.0-1) unstable; urgency=medium

  * Upload to unstable

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 18 Jun 2017 19:29:54 +0200

openmprtl (4.0-1~exp1) experimental; urgency=medium

  * new testing release
  * removed openmprtl_add-mips-support.diff
     add-mips-support-z_Linux_util.c.patch
     mips-support.diff
     Merged upstream
  * update the symbol list with *_fp

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 21 Jan 2017 16:39:58 +0100

openmprtl (3.9.1-1) unstable; urgency=medium

  * New upstream release
  * d/p/force-lang-c.diff removed
    applied upstream
  * update the list of symbol for ppc64el & ppc64 (Closes: #847758)

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 12 Nov 2016 10:18:53 +0100

openmprtl (3.9.0-2) unstable; urgency=medium

  * Fix the FTBFS on mips*. Thanks to Radovan Birdic for the patch
    (Closes: #830956)
  * dpkg-buildpackage -A is now working correctly (Closes: #806089)

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 12 Nov 2016 09:28:14 +0100

openmprtl (3.9.0-1) unstable; urgency=medium

  * New upstream release
    New symbols:
      - kmp_aligned_malloc
      - kmpc_aligned_malloc
      - kmpc_set_disp_num_buffers
      - kmp_set_disp_num_buffers
      - omp_get_max_task_priority
      - omp_get_num_places
      - omp_get_partition_num_places
      - omp_get_partition_place_nums
      - omp_get_place_num
      - omp_get_place_num_procs
      - omp_get_place_proc_ids
  * Update of the url to http://openmp.llvm.org/
  * debian/watch updated to reflect the changes
  * compat udpdated to 9

 -- Sylvestre Ledru <sylvestre@debian.org>  Fri, 11 Nov 2016 16:31:06 +0100

openmprtl (3.8.0-2) unstable; urgency=medium

  * Reintegrate some missing changes

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 02 Jul 2016 22:43:26 +0200

openmprtl (3.8.0-1) unstable; urgency=medium

  * New upstream release
  * Standards-Version updated to 3.9.8

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 02 Jul 2016 22:11:30 +0200

openmprtl (3.7.0-3) unstable; urgency=medium

  * Fix the support of mips/mipsel.
    Thanks to Dejan Latinovic (Closes: #790735)

 -- Sylvestre Ledru <sylvestre@debian.org>  Wed, 21 Oct 2015 15:56:50 +0200

openmprtl (3.7.0-2) unstable; urgency=medium

  [ Andreas Beckmann ]
  * Set SONAME to libiomp.so.5 properly (Closes: #775257)
  * Ship a libiomp5.so compat symlink.
  * Convert to multiarch.

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 20 Oct 2015 13:57:07 +0200

openmprtl (3.7.0-1) unstable; urgency=medium

  * New upstream release
  * Move to the LLVM version of the library (Closes: ##792180)
  * Rename the packages: s/iomp/omp/g
  * Fix the Vcs-* fields (Closes: #792179)
  * Update of the copyright file
  * Support of MIPS (Closes: #790735)
    Thanks to Dejan Latinovic for the patches

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 15 Sep 2015 15:58:03 +0200

openmprtl (0.20150701-1) unstable; urgency=medium

  * New upstream release
  * Include the fix to build with gcc 5 (Closes: #778046)

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 16 Jun 2015 18:11:19 +0200

openmprtl (0.20150401-1) unstable; urgency=medium

  * Upload to unstable
  * New upstream release
  * New symbols (kmp_debugging, kmp_omp_debug_struct_info,
    kmpc_proxy_task_completed, kmpc_proxy_task_completed_ooo

 -- Sylvestre Ledru <sylvestre@debian.org>  Mon, 27 Apr 2015 10:17:30 +0200

openmprtl (0.20141212-1~exp2) experimental; urgency=medium

  * Make the build reproducible (remove a date in the header +
    timestamp in the doc)

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 31 Jan 2015 16:49:42 +0100

openmprtl (0.20141212-1~exp1) experimental; urgency=medium

  * New upstream release
  * Standards-Version updated to 3.9.6
  * 3 new symbols added (omp_is_initial_device*)
  * Patches refreshed

 -- Sylvestre Ledru <sylvestre@debian.org>  Wed, 31 Dec 2014 13:13:25 +0100

openmprtl (0.20140926-1) unstable; urgency=medium

  * New upstream release

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 07 Oct 2014 14:56:59 +0200

openmprtl (0.20131209-1) unstable; urgency=low

  * New upstrem release
  * Standards-Version updated to 3.9.5
  * Update libiomp5.symbols. Introduction of @OMP_1.0 & @OMP_3.0

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 19 Jan 2014 16:40:56 +0100

openmprtl (0.20130912-1) unstable; urgency=low

  * New upstream release
  * README => README.txt
  * Two new symbols added kmpc_omp_task_with_deps & kmpc_omp_wait_deps

 -- Sylvestre Ledru <sylvestre@debian.org>  Mon, 30 Sep 2013 16:53:17 +0200

openmprtl (0.20130715-1) unstable; urgency=low

  * Initial release (Closes: #706599)

 -- Sylvestre Ledru <sylvestre@debian.org>  Thu, 30 May 2013 11:38:31 +0200
